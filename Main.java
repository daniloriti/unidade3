import java.util.*;

public class Main {
    
    public static ArrayList<Integer> lista;
    public static ArrayDeque<Integer> pilha;
    public static ArrayDeque<Integer> fila;

    public static void main(String args[]) {

        System.out.println("------------------------------------------------------------------------");
        System.out.println("Passo 1: Insira os números [1, 2, 3, 4 e 5] em uma lista - com 5 células");

        int[] numeros = new int[] { 1, 2, 3, 4, 5 };
        
        lista = new ArrayList<Integer>(5);

        InserirNumerosLista(numeros);
        

        System.out.println("                                                                        ");
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Passo 2: Remova todos os dados da lista e insira-os em uma Pilha - com 5 células. Deve-se sempre remover os dados da célula inicial da lista");
    
        pilha = new ArrayDeque<Integer>(5);
        
        RemoverNumerosListaInserirNumerosPilha();
        
        
        System.out.println("                                                                        ");
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Passo 3: Remova os dados da Pilha e insira-os em uma Fila - com 10 células");
    
        fila = new ArrayDeque<Integer>(10);
    
        RemoverNumerosPilhaInserirNumerosFila();
        
        
        System.out.println("                                                                        ");
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Passo 4: Insira os números [6, 7, 8, 9 e 10] na lista");
    
        int[] numerosContinuacao = new int[] { 6, 7, 8, 9, 10 };
    
        InserirNumerosLista(numerosContinuacao);
        
        
        System.out.println("                                                                        ");
        System.out.println("------------------------------------------------------------------------");
        System.out.println("Passo 5: Repita os passos 2 e 3.");
    
        RemoverNumerosListaInserirNumerosPilha();
        RemoverNumerosPilhaInserirNumerosFila();
   }
   
    public static void InserirNumerosLista(int[] numeros) {
        System.out.println("- Inserindo os números em uma lista...");
        
        for (int i = 0; i < numeros.length; i++){
    	    lista.add(numeros[i]);
    		System.out.println("- Número " + lista.get(lista.size() - 1) + " adicionado");
    	}

        System.out.println("- Resultado da lista: " + lista);
    }
    
    public static void RemoverNumerosListaInserirNumerosPilha() {
        System.out.println("- Removendo os números da lista e adicionando a uma pilha...");
        
        while(!lista.isEmpty()) {
            int numero = lista.remove(0);
            System.out.println("- Número " + numero + " removido da lista");
            
            System.out.println("- Resultado da lista: " + lista);
            
            pilha.push(numero);
            System.out.println("- Número " + pilha.peek() + " adicionado a pilha");
            System.out.println("- ----------------------------------------------");
        }
        
        System.out.println("- Resultado da pilha: " + pilha);
    }
    
    public static void RemoverNumerosPilhaInserirNumerosFila() {
        System.out.println("- Removendo os números da pilha e adicionando a uma fila de 10 células...");
        
        while(!pilha.isEmpty()) {
            int numero = pilha.pop();
            System.out.println("- Número " + numero + " removido da pilha");
            
            System.out.println("- Resultado da pilha: " + pilha);
            
            fila.add(numero);
            System.out.println("- Número " + fila.getLast() + " adicionado a fila");
            System.out.println("- ----------------------------------------------");
        }
        
        System.out.println("- Resultado da fila: " + fila);
    }
}